# Работа в CLion

## Начальная настройка

### Шаг 0

[Установите CLion](https://www.jetbrains.com/ru-ru/clion/).

### Шаг 1

Курс – это CMake-проект, так что просто откройте его в CLion: `File` > `Open...` > выбрать директорию локального репозитория курса.

### Шаг 2

Настраиваем [Remote development](https://www.jetbrains.com/help/clion/remote-projects-support.html).

В `Preferences` > `Build, Execution, Deployment` > `Toolchains` создайте Remote Host и заполните поля как на скриншоте:

![Setup remote host](images/toolchain.png)

#### 2.1 Credentials

Нажмите на шестеренку в поле `Credentials` и создайте `SSH Configuration` заполняя следующие поля: 

| Поле | Значение |
| - | - |
| _Host_ | `127.0.0.1` |
| _Port_ | `2222`
| _User name_ | `clion_user` |
| _Password_ | `password` |

![Setup remote host](images/credentials.png)

#### 2.2 Tools

Заполните поля:

| Поле | Значение |
| - | - |
| _Make_ | `/usr/bin/make` |
| _C Compiler_ | `/usr/bin/clang-10`
| _C++ Compiler_ | `/usr/bin/clang++-10` |

Проверьте, что в поле `Make` вы написали путь именно к `make`, а не к `cmake`.

### Шаг 3

В `Preferences` > `Build, Execution, Deployment` > `CMake` добавьте новый профиль сборки и установите в нем созданный шагом ранее тулчейн:

![Setup remote host](images/profile.png)

### Шаг 4 (только для CLion >= 2021.2)

В этот момент CLion начнёт синхронизацию файлов между хост системой и ремоутом
и будет делать это каждый раз при изменении файлов проекта (в табе `File 
Transfer` будет много логов о загрузке файлов в `/tmp/tmp...`).

Этого можно избежать, если у вас установлен CLion 
[версии >= 2021.2](https://blog.jetbrains.com/clion/2021/06/clion-2021-2-eap-remote-dev-debugger-cmake-presets/#remote_dev_docker).

В `Preferences` > `Build, Execution, Deployment` > `Deployment` появился новый
деплоймент, который был сгенерирован автоматически CLion'ом:

![Auto deployment](images/deployment/auto.png)

Измените его тип на `Local or mounted folder`:

![Deployment type](images/deployment/docker-type.png)

И соотнесите путь до локального воркспейса с путём до воркспейса внутри 
ремоута:

![Deployment mappings](images/deployment/docker-mappings.png)

В этот момент CLion должен перестать постоянно загружать файлы в ремоут.
Убедиться в этом можно, проверив таб `File Transfer`, -- никаких новых
логов появляться там не должно.

### Шаг 5

Готово! Теперь можно выбрать в IDE цель с задачей/тестами и запустить её!

## Полезные советы

- В любой непонятной ситуации с удаленной сборкой следует попробовать следующее заклинание:

ПКМ по корневой папке репозитория > `Deployment` > `Upload to...`

- В окошке `Terminal` можно залогиниться в контейнер и работать там с консольным клиентом `clippy`
