#include <rsm/raft/request_id.hpp>

#include <tuple>

namespace raft {

std::ostream& operator<<(std::ostream& out, const RequestId& id) {
  out << id.client_id << "--" << id.request_index;
  return out;
}

bool operator==(const RequestId& lhs, const RequestId& rhs) {
  return std::tie(lhs.client_id, lhs.request_index) ==
         std::tie(rhs.client_id, rhs.request_index);
}
bool operator!=(const RequestId& lhs, const RequestId& rhs) {
  return !(lhs == rhs);
}

bool operator<(const RequestId& lhs, const RequestId& rhs) {
  return std::tie(lhs.client_id, lhs.request_index) <
         std::tie(rhs.client_id, rhs.request_index);
}

}  // namespace raft
