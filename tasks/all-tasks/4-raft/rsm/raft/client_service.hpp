#pragma once

#include <rsm/raft/rsm.hpp>

#include <whirl/node/node_methods_base.hpp>
#include <whirl/node/services.hpp>
#include <whirl/node/logging.hpp>

#include <whirl/rpc/use/service_base.hpp>

#include <await/fibers/sync/future.hpp>
#include <await/fibers/core/await.hpp>

namespace raft {

class ClientService : public whirl::rpc::ServiceBase<ClientService>,
                      public whirl::NodeMethodsBase {
 public:
  ClientService(whirl::NodeServices runtime, IReplicatedStateMachinePtr rsm)
      : NodeMethodsBase(runtime), rsm_(std::move(rsm)) {
  }

  void RegisterRPCMethods() override {
    RPC_REGISTER_METHOD(Execute);
  }

 protected:
  RSMResponse Execute(Command command) {
    NODE_LOG("Execute client command: {}", command);
    return await::fibers::Await(rsm_->Execute(command)).Value();
  }

 private:
  IReplicatedStateMachinePtr rsm_;
};

}  // namespace raft
