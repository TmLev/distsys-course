#pragma once

#include <rsm/raft/command.hpp>

#include <memory>

namespace raft {

struct IStateMachine {
  virtual ~IStateMachine() = default;

  // Returns serialized operation response
  // For simplicity we do not care about errors
  virtual Bytes Apply(const Command& command) = 0;
};

using IStateMachinePtr = std::shared_ptr<IStateMachine>;

}  // namespace raft
