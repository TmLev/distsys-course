#pragma once

#include <rsm/kv/client/client.hpp>

#include <whirl/matrix/history/record_call.hpp>

namespace whirl {

class RecordingKVClient {
 public:
  RecordingKVClient(kv::BlockingClient kv) : kv_(std::move(kv)) {
  }

  // NOLINTNEXTLINE
  void Set(kv::Key key, kv::Value value) {
    WHIRL_RECORDED_VOID_CALL(kv_, Set, key, value)  // NOLINT
  }

  // NOLINTNEXTLINE
  kv::Value Get(kv::Key key) {
    WHIRL_RECORDED_CALL(kv_, Get, key)  // NOLINT
  }

  // NOLINTNEXTLINE
  kv::Value Cas(kv::Key key, kv::Value expected, kv::Value target) {
    WHIRL_RECORDED_CALL(kv_, Cas, key, expected, target)  // NOLINT
  }

 private:
  kv::BlockingClient kv_;
};

}  // namespace whirl
