#pragma once

#include <whirl/matrix/world/global/random.hpp>
#include <whirl/matrix/world/global/time.hpp>

namespace whirl {

class RaftWorldBehaviour : public IWorldBehaviour {
  // Time

  // [-10, +10]
  int InitClockDrift() override {
    return -10 + (int)GlobalRandomNumber(20);
  }

  int ClockDriftBound() override {
    return 20;
  }

  TimePoint GlobalStartTime() override {
    return GlobalRandomNumber(1000);
  }

  TimePoint ResetMonotonicClock() override {
    return GlobalRandomNumber(1, 100);
  }

  TimePoint InitLocalClockOffset() override {
    return GlobalRandomNumber(1000);
  }

  Duration TrueTimeUncertainty() override {
    return GlobalRandomNumber(5, 500);
  }

  // Network

  TimePoint FlightTime(const net::Packet& packet) override {
    if (packet.type != net::EPacketType::Data) {
      // Service packet, do not affect randomness
      return 50;
    }

    if (GlobalRandomNumber() % 5 == 0) {
      return GlobalRandomNumber(30, 200);
    }
    return GlobalRandomNumber(30, 60);
  }
};

}  // namespace whirl
