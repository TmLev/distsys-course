#pragma once

#include <rsm/kv/store/types.hpp>
#include <rsm/kv/proto/proto.hpp>

#include <rsm/raft/client.hpp>

namespace kv {

////////////////////////////////////////////////////////////////////////////////

class BlockingClient {
 public:
  BlockingClient(raft::BlockingClient&& impl) : impl_(std::move(impl)) {
  }

  void Set(Key key, Value value) {
    impl_.Execute<proto::Set>("Set", proto::Set::Request{key, value});
  }

  Value Get(Key key) {
    auto response = impl_.Execute<proto::Get>("Get", proto::Get::Request{key});
    return response.value;
  }

  Value Cas(Key key, Value expected, Value target) {
    auto response = impl_.Execute<proto::Cas>(
        "Cas", proto::Cas::Request{key, expected, target});
    return response.old_value;
  }

 private:
  raft::BlockingClient impl_;
};

}  // namespace kv
