# KV

Реализуйте отказоустойчивое линеаризуемое KV хранилище.

## Сервисы

Система должна предоставлять клиентам RPC-сервис `KV` с методами:

| Сигнатура | Семантика |
| - | - |
| `void Set(Key key, Value value)` | Записать значение `value` по ключу `key` |
| `Value Get(Key key)` | Прочитать значение по ключу `key` |

## Отказоустойчивость

Система должна переживать:

- Рестарты узлов
- Партишены сети
- Отказ произвольного меньшинства узлов

## Модель согласованности

[Линеаризуемость](https://jepsen.io/consistency/models/linearizable)

## Алгоритм 

Используйте алгоритм _ABD_ (_Attiya_, _Bar-Noy_, _Dolev_) репликации атомарного регистра.

## Шаблон решения

[Вот он!](kv/node)

## Указания по реализации

### Concurrency

Один узел может конкурентно обслуживать разные вызовы `LocalWrite` и `LocalRead`. 

Обеспечьте атомарность обработчиков этих вызовов с помощью [`await::fibers::Mutex`](https://gitlab.com/Lipovsky/await/-/blob/master/await/fibers/sync/mutex.hpp).

### Write timestamp

Выбирать временную метку для записи можно двумя способами:

1) С помощью дополнительной фазы кворумного чтения
2) Локально с помощью сервиса [`TrueTime`](https://gitlab.com/Lipovsky/whirl/-/blob/master/whirl/services/true_time.hpp)
