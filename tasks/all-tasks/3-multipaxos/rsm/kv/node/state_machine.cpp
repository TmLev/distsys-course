#include <rsm/kv/node/state_machine.hpp>

#include <rsm/kv/proto/proto.hpp>

namespace kv {

// Create Result from value
using wheels::make_result::Ok;

Result<CommandOutput> StateMachine::Apply(const Command& command) {
  if (command.name == "Set") {
    auto [set] = command.input.As<SetRequest>();
    kv_.Set(set.key, set.value);
    return Ok(CommandOutput::MakeVoid());

  } else if (command.name == "Get") {
    auto [get] = command.input.As<GetRequest>();
    auto value = kv_.Get(get.key);
    return Ok(CommandOutput::Make(value));

  } else if (command.name == "Cas") {
    auto [cas] = command.input.As<CasRequest>();
    auto old_value = kv_.Cas(cas.key, cas.expected_value, cas.target_value);
    return Ok(CommandOutput::Make(old_value));

  }

  // TODO: Support Nop command
  // (MakeNopCommand from multipaxos/command.hpp)

  // Supress compiler warning
  WHEELS_UNREACHABLE();
}

}  // namespace kv
