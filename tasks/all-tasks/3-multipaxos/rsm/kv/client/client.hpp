#pragma once

#include <rsm/kv/store/types.hpp>
#include <rsm/kv/proto/proto.hpp>

#include <whirl/rpc/use/channel.hpp>

namespace kv {

using whirl::rpc::TChannel;

class BlockingClient {
 public:
  BlockingClient(TChannel channel, const std::string& client_id);

  void Set(Key key, Value value);
  Value Get(Key key);
  Value Cas(Key key, Value expected, Value target);

 private:
  RequestId NextRequestId();

 private:
  TChannel rpc_channel_;
  std::string client_id_;
  size_t request_index_{0};
};

}  // namespace kv
