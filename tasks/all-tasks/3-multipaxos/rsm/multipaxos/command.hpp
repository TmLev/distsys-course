#pragma once

#include <whirl/cereal/serialize.hpp>

#include <cereal/types/string.hpp>
#include <cereal/types/tuple.hpp>

#include <string>
#include <ostream>

namespace multipaxos {

//////////////////////////////////////////////////////////////////////

using CommandId = std::string;

//////////////////////////////////////////////////////////////////////

using Bytes = std::string;

//////////////////////////////////////////////////////////////////////

/*
 * Command input
 * Usage:
 * 1) Create from arguments:
 * auto input = CommandInput::Make(key, value);
 * 2) Unpack:
 * auto [key, value] = input.As<Key, Value>();
 */

class CommandInput {
 public:
  CommandInput(Bytes&& raw_bytes) : raw_bytes_(std::move(raw_bytes)) {
  }

  // For serialization
  CommandInput() = default;

  template <typename... Types>
  static CommandInput Make(Types&&... values) {
    auto packed_values = std::make_tuple(std::forward<Types>(values)...);
    auto input = whirl::Serialize(packed_values);
    return CommandInput{std::move(input)};
  }

  template <typename... Types>
  std::tuple<Types...> As() const {
    return whirl::Deserialize<std::tuple<Types...>>(raw_bytes_);
  }

  WHIRL_SERIALIZE(raw_bytes_)

 private:
  Bytes raw_bytes_;
};

//////////////////////////////////////////////////////////////////////

struct Command {
  CommandId id;
  std::string name;
  CommandInput input;

  bool operator==(const Command& that) const {
    return id == that.id;
  }
  bool operator!=(const Command& that) const {
    return !(*this == that);
  }

  WHIRL_SERIALIZE(CEREAL_NVP(id), CEREAL_NVP(name), CEREAL_NVP(input))
};

std::ostream& operator<<(std::ostream& out, const Command& command);

//////////////////////////////////////////////////////////////////////

// Nop command
// https://en.wikipedia.org/wiki/NOP_(code)
Command MakeNopCommand();

//////////////////////////////////////////////////////////////////////

// Serialized command output

struct CommandOutput {
 public:
  CommandOutput(Bytes&& bytes) : raw_bytes_(std::move(bytes)) {
  }

  // For serialization
  CommandOutput() = default;

  template <typename T>
  static CommandOutput Make(T value) {
    return CommandOutput{whirl::Serialize<T>(value)};
  }

  static CommandOutput MakeVoid() {
    return CommandOutput{std::string("")};
  }

  template <typename T>
  T As() const {
    return whirl::Deserialize<T>(raw_bytes_);
  }

  WHIRL_SERIALIZE(raw_bytes_)

 private:
  Bytes raw_bytes_;
};

}  // namespace multipaxos
