#pragma once

#include <rsm/multipaxos/log_entry.hpp>

#include <whirl/services/local_storage.hpp>

#include <string>
#include <memory>

namespace multipaxos {

using whirl::ILocalStorageBackendPtr;

//////////////////////////////////////////////////////////////////////

class PersistentLog {
 public:
  PersistentLog(ILocalStorageBackendPtr storage, const std::string& name);

  // Returns LogEntry::Empty() if slot is empty
  LogEntry Read(size_t index);

  void Write(size_t index, LogEntry entry);

  bool IsEmpty(size_t index) const;

 private:
  static std::string MakeKey(size_t index);
  static std::string MakeLogName(const std::string& name);

 private:
  // Indicies -> LogEntries
  whirl::LocalKVStorage<LogEntry> impl_;
};

using PersistentLogPtr = std::shared_ptr<PersistentLog>;

//////////////////////////////////////////////////////////////////////

// Use RSM name
PersistentLogPtr OpenLog(ILocalStorageBackendPtr storage,
                         const std::string& name);

}  // namespace multipaxos
